This is a Features based Feeds Module importer It will allow you to import The Holy Quran in csv format to your Drupal Website.
 The csv files are based here at https://github.com/azvox/quran-csv/blob/master/resources/quranall-online.... Other choices are available depending on your taste.

To install, just download and enable it. Click on import quran link on the user menu, upload your downloaded csv file and click import. It may take a while, after it finishes, you will get a fully functioning site with translations and recitation at least three recitations. You will also get more than thirty translations and recitations of the Quran. 
Just change the views included to show the recitations or translations that you want.

To configure you can watch the youtube video here https://www.youtube.com/watch?v=JpFdCRcWCIQ